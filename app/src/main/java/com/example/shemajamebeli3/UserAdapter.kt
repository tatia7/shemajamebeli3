package com.example.shemajamebeli3

import android.app.AlertDialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import org.w3c.dom.Text

class UserAdapter(private val users:MutableList<Users>,private val Listener: remUpListener) :
    RecyclerView.Adapter<UserAdapter.ItemViewHolder>() {

    inner class ItemViewHolder(val v: View):RecyclerView.ViewHolder(v), View.OnClickListener{
        private lateinit var firstname : TextView
        private lateinit var lastname : TextView
        private lateinit var email : TextView
        private lateinit var user : Users
        private lateinit var btnUpdate : ImageButton
        private lateinit var btnRemove : ImageButton
        fun widgets(){
            user = users[adapterPosition]
            firstname.text = user.firstName
            lastname.text = user.lastName
            email.text = user.Email
            btnRemove.setOnClickListener(this)
            btnUpdate.setOnClickListener(this)
        }
        override fun onClick(v: View?) {
            Listener.removeUser(adapterPosition)
            Listener.updateUser(adapterPosition)
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val v = inflater.inflate(R.layout.activity_users,parent,false)
        return ItemViewHolder(v)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.widgets()
    }

    override fun getItemCount(): Int {
        return users.size
    }
}