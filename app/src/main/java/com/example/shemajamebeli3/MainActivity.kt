package com.example.shemajamebeli3

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.Telephony
import android.view.LayoutInflater
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton

class MainActivity : AppCompatActivity() {

    private lateinit var addBtn: ImageButton
    private lateinit var recView: RecyclerView
    private lateinit var users: ArrayList<Users>
    private lateinit var userAdapter: UserAdapter
    private lateinit var name : TextView
    private lateinit var surname : TextView
    private lateinit var email : TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
    }
    private fun init(){
        addBtn = findViewById(R.id.addingBtn)
        recView = findViewById(R.id.mRecycler)
        name = findViewById(R.id.firstname)
        surname = findViewById(R.id.lastname)
        email = findViewById(R.id.email)
        users = ArrayList()
        userAdapter = UserAdapter(users, object : remUpListener{
            override fun removeUser(position: Int) {
                users.removeAt(position)
                Toast.makeText(this@MainActivity, "User has been removed", Toast.LENGTH_LONG).show()
                userAdapter.notifyItemRemoved(position)
            }

            override fun updateUser(position: Int) {
                updateActivity()
            }
        })
        recView.layoutManager = LinearLayoutManager(this)
        recView.adapter = userAdapter
        addBtn.setOnClickListener{addInfo()}
    }
    private fun updateActivity(){
        val intent = Intent(this, UserActivity::class.java)
        startActivityForResult(intent,11)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(requestCode == 11 && resultCode == RESULT_OK){
            val Name = data?.extras?.getString("Name")
            val Surname = data?.extras?.getString("Surname")
            val Email = data?.extras?.getString("Email")

            name.setText(Name)
            surname.setText(Surname)
            email.setText(Email)
        }
        super.onActivityResult(requestCode, resultCode, data)
    }
    private fun addInfo(){
        val inflter = LayoutInflater.from(this)
        val v = inflter.inflate(R.layout.add_user, null)
        val addDialog = AlertDialog.Builder(this)
        val Name = v.findViewById<EditText>(R.id.userFirstName)
        val Sur = v.findViewById<EditText>(R.id.userLastName)
        val Email = v.findViewById<EditText>(R.id.userEmail)

        addDialog.setView(v)
        addDialog.setPositiveButton("Ok"){
                dialog,_->
            val names = Name.text.toString()
            val surrs = Sur.text.toString()
            val emails = Email.text.toString()
            users.add(Users("tatia","siria","sdada"))
            userAdapter.notifyDataSetChanged()
            Toast.makeText(this, "Success", Toast.LENGTH_LONG).show()
            dialog.dismiss()
        }
        addDialog.setNegativeButton("Cancel"){
                dialog,_->
            dialog.dismiss()
            Toast.makeText(this,"Cancel", Toast.LENGTH_LONG).show()
        }
        addDialog.create()
        addDialog.show()
    }
}
